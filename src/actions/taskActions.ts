import { ADD_TASK, CHECK_TASK, DELETE_TASK } from "./actionTypes"




export const addTask = (data:string) => {
    
    return {
        type: ADD_TASK,
        data
    }
}
export const deleteTask = (id:number) => {
    return {
        type: DELETE_TASK,
        id
    }
}

export const checkTask = (id:number) => {
    return{
        type: CHECK_TASK,
        id
    }
}