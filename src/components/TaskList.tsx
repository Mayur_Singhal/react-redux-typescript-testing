import { useState,useEffect } from "react"
import { useSelector } from "react-redux"
import TaskItem from "./TaskItem"

import {taskType}from '../types'

type stateType = {
    tasks:taskType[]
}

const TaskList = () => {

    const tasks:taskType[] = useSelector((state:stateType) => state.tasks)

    const [list ,setList] = useState<taskType[]>([])
    
    useEffect(() => {
        setList(tasks)
    }, [tasks])
    
    
    return (
        <div data-testid="container" >
            { list.length !== 0 ?  
                list.map((task,index) => 
                    <TaskItem  key={index} task={task}/> ) : 
                <h1 id="header">No task lefts</h1> }
        </div>
    )
}

export default TaskList
