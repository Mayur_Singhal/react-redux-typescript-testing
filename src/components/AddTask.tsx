import {  useState } from "react"
import { useDispatch } from "react-redux";
import { addTask } from "../actions/taskActions";

const AddTask = () => {

    const [title,setTitle] = useState('');
    const [error,setError] = useState('Title cannot be blank');
    const dispatch = useDispatch()
    const handleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        setTitle(e.target.value)
        
        
    }
    
    const handleSubmit = (e:React.FormEvent) => {

        e.preventDefault();
        
        let valid = true;
        if(title === ''){
            setError('Title cannot be blank')
            alert(error)
            valid = false;
        }
        
        if(valid){
            const data = title;
            console.log(data)
            dispatch(addTask(data))
            setTitle('')
        }
    }
    
    return (
        <form onSubmit={handleSubmit}> 
            
            <input 
                type="text" 
                placeholder="What need to be done"
                value = {title}
                onChange={handleChange} 
                
                />
        </form>
    )
}

export default AddTask
