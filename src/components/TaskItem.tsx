
import { useDispatch } from "react-redux"
import { checkTask, deleteTask } from "../actions/taskActions"
import { taskType } from "../types"

type TaskItemType = {
    key:number,
    task:taskType
}

const TaskItem = ({task}:TaskItemType) => {
    
    const dispatch = useDispatch()
   
    const handledeleteTask = () => {
        dispatch(deleteTask(task.id))
    } 
    const handleCheck = () => {
        console.log(task.id)
        dispatch(checkTask(task.id))

    }
    const check = task.completed 
    return (
        <div data-testid="alltask" className="taskItem">
            <input 
                type="checkbox" 
                onClick = {handleCheck}
                 />
            
            <p style= {check ? {
                textDecoration:"line-through",color:'rgb(128, 125, 125)'} : 
                {textDecoration:"none"} }
                > {task.title}</p>
            
            <button onClick={handledeleteTask} >
              
               X
             
            </button>
            
        </div>
    )
}

export default TaskItem
