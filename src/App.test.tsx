import {render,screen,fireEvent} from '@testing-library/react'
import { Provider } from 'react-redux'
import App from './App'
import store from './store'


describe("My List",() =>{
   
    beforeEach(() => {
        render(<Provider store={store} >
            <App/>
        </Provider>)
    })
    test('Header Component', () => {
        expect(screen.getByRole('heading',{name:'my list'})).toBeInTheDocument
    })
    test("THEN there are no todos shown", () => {
        expect(screen.getByTestId('container')).toHaveTextContent(/No task lefts/i);
    });

    
    describe("AND when a todo is added", () => {
        
        test("initilay input is empty",()=> {
            expect(screen.getByPlaceholderText('What need to be done')).toHaveTextContent('')
        })

        test("input should change its value",() => {
            const inputElement = screen.getByPlaceholderText('What need to be done');
            fireEvent.change(inputElement,{
                target:{value:'My first testing task'}
            })
            expect(inputElement).toHaveValue('My first testing task')
        })
        

        test("THEN the todo is visible in list", () => {
            const inputElement = screen.getByPlaceholderText('What need to be done');
            fireEvent.change(inputElement,{
                target:{value:'My first testing task'}
            })
            fireEvent.submit(inputElement)
            expect(screen.getByTestId('container')).toHaveTextContent("My first testing task");
        });

        test("onClick set complete 'ture' ", () => {
            const taskElement = screen.getByText("My first testing task");
            expect(taskElement).toHaveAttribute('style','text-decoration: none;')            
            
        })

        test("onClick set complete 'ture' ", () => {
            const checkElement = screen.getByRole('checkbox');
            fireEvent.click(checkElement);
            const taskElement = screen.getByText("My first testing task");
            expect(taskElement).toHaveAttribute('style','text-decoration: line-through; color: rgb(128, 125, 125);')            
        })

        test("add one more task", () => {
            const inputElement = screen.getByPlaceholderText('What need to be done');
            fireEvent.change(inputElement,{
                target:{value:'My Second testing task'}
            })
            fireEvent.submit(inputElement)
            expect(screen.getByTestId('container')).toHaveTextContent("My Second testing task");
            expect(screen.getAllByTestId("alltask").length).toBe(2)
        })
        
        test("on hover shows close button", () => {
            const taskElement = screen.getByText("My first testing task");
            fireEvent.mouseOver(taskElement);
            expect(screen.getAllByRole('button',{name:'X'})).toBeTruthy
        })

        test("onClick 'X' button task should be deleted", () => {
            expect(screen.getAllByTestId("alltask").length).toBe(2)
            const taskElement = screen.getByText("My first testing task");
            fireEvent.mouseOver(taskElement);
            const deleteButton = screen.getAllByRole('button',{name:'X'})
            fireEvent.click(deleteButton[0]);
            expect(screen.getAllByTestId("alltask").length).toBe(1)

        })
    });
  

})