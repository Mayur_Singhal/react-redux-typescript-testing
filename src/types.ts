export type taskType = {
    id:number,
    title:string,
    completed:Boolean
}