import Header from './components/Header';
import TaskManger from './components/TaskManager';

const App = () => {
    return (
        <div className="App">
            <Header/>
            <TaskManger/>
        </div>
    )
}

export default App
