import { AnyAction } from "redux";
import { ADD_TASK, CHECK_TASK,DELETE_TASK } from "../actions/actionTypes";

import {taskType} from '../types'

type taskArray = {
    tasks:taskType[]
}

const intitalState:taskArray = {
    tasks : [
        
    ]
}



export const taskReducer = (state= intitalState,action:AnyAction) => {
    switch(action.type){

        case ADD_TASK :
            const task:taskType = {
                id:state.tasks.length+1,
                title:action.data,
                completed:false
            }
            
            return {
               tasks: [...state.tasks,task]
            }

        case DELETE_TASK :
            
            return {
                tasks: state.tasks.filter(task => task.id !== action.id)
            }

        case CHECK_TASK:
            const tasks = state.tasks.map(obj =>{
                if(obj.id === action.id){
                    obj.completed = !obj.completed
                }
                return obj
            })
            
            return {
                ...state,
                tasks
            }

        default :
            return state;
    }
}
